let lastNameData = {
  name: "Arya",
  lastName: "Stark",
};

let firstName = (data) => {
  let err = false;
  return new Promise((resolve, reject) => {
    if (!err) {
      resolve(data["name"]);
    } else {
      reject("data not Found");
    }
  });
};
firstName(lastNameData).then((data) => {
  console.log(data);
});

let lastName = (data) => {
  let err = false;
  return new Promise((resolve, reject) => {
    if (!err) {
      resolve(data["lastName"]);
    } else {
      reject("data not Found");
    }
  });
};
lastName(lastNameData).then((data) => {
  console.log(data);
});

let fullName = (data) => {
  Promise.all([firstName(data), lastName(data)])
    .then((ele) => {
      console.log(ele[0] + " " + ele[1]);
    })
    .catch((err) => console.log(err));
};
fullName(lastNameData);
