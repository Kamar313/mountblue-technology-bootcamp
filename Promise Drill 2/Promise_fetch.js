let url = "https://jsonplaceholder.typicode.com/users";

async function fetchApi(api) {
  let res = await fetch(api);
  let data = await res.json();
  console.log(data.slice(0, 10));
}
fetchApi(url);

function promise(api) {
  fetch(api)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      console.log(data.slice(0, 10));
    });
}
promise(url);
