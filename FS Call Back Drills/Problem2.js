const fs = require("fs");
const { json } = require("stream/consumers");

let getData = fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
  if (err) {
    console.error(err);
  }
  fs.writeFile(
    "./Upper and LowerCase/UpperCase.txt",
    data.toUpperCase(),
    (err) => {
      if (err) {
        throw err;
      }
      fs.readFile(
        "./Upper and LowerCase/UpperCase.txt",
        "utf-8",
        (err, data) => {
          if (err) {
            console.error(err);
          }
          fs.writeFile(
            "./Upper and LowerCase/LowerCase.txt",
            data.toLowerCase().split(".").join("\n"),
            (err) => {
              if (err) {
                console.error(err);
              }
              fs.readFile(
                "./Upper and LowerCase/LowerCase.txt",
                "utf-8",
                (err, data) => {
                  if (err) {
                    console.error(err);
                  }
                  fs.writeFile(
                    "./Upper and LowerCase/sort.txt",
                    data.split("\n").sort().join(""),
                    (err) => {
                      if (err) {
                        console.error(err);
                      }
                      fs.readdir(
                        "./Upper and LowerCase",
                        "utf-8",
                        (err, data) => {
                          if (err) {
                            console.error(err);
                          }
                          fs.writeFile(
                            "./Upper and LowerCase/filenames.txt",
                            JSON.stringify(data),
                            (err) => {
                              if (err) {
                                console.error(err);
                              }
                              fs.readFile(
                                "./Upper and LowerCase/filenames.txt",
                                "utf-8",
                                (err, data) => {
                                  if (err) {
                                    console.error(err);
                                  }
                                  let a = JSON.parse(data);
                                  a.map((e) => {
                                    fs.unlink(
                                      `./Upper and LowerCase/${e}`,
                                      (err) => {
                                        if (err) {
                                          console.error(err);
                                        }
                                        console.log("data Deleted");
                                      }
                                    );
                                  });
                                }
                              );
                            }
                          );
                        }
                      );
                    }
                  );
                }
              );
            }
          );
        }
      );
    }
  );
});
module.exports = getData;
