const fs = require("fs");

function getData() {
  if (!fs.existsSync("Json Fils")) {
    fs.mkdir("Json Fils", (err) => {
      throw err;
    });
  } else {
    fs.readFile("./data.json", "utf8", function (err, data) {
      if (err) {
        throw err;
      } else {
        let a = Math.random() * 20;
        fs.appendFile(`./Json Fils/${a}.json`, data, "utf8", (err) => {
          if (err) {
            throw err;
          }
          fs.readdir("./Json Fils", function (err, files) {
            if (err) {
              throw err;
            }
            files.map((e) => {
              fs.unlink(`Json Fils/${e}`, (err) => {
                if (err) {
                  throw err;
                }
                console.log("Fils Deleted");
              });
            });
          });
        });
      }
    });
  }

  // if(fs.existsSync("FS Call Back Drills/Json Fils")){
  // for(let i=0;i<10;i++){
  //     fs.writeFile(`FS Call Back Drills/Json Fils/new${[i]}.json`,JSON.stringify(data,null,2),err=>{
  //         if(err){
  //             console.log("error")
  //             console.log(err)
  //         }else{
  //             fs.unlink(`FS Call Back Drills/Json Fils/new${[i]}.json`,err=>{
  //                 if(err){
  //                     console.log("Not Deleted")
  //                 }else{
  //                     console.log("file is Deleted")
  //                 }
  //         })}
  //     })
  // }}else{
  //     console.log("Aleardy Directory Persent")
  // }
}
module.exports = getData;
