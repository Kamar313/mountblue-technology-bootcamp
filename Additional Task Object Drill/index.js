
const got= require('./data')

function countAllPeople(get) {
  let getCount=get.houses.map((key)=>{
    return key.people.length
  })
  let count=0;
  getCount.forEach((curr)=>{
    count+=curr
  })
  return count
}

function peopleByHouses(getPeople) {
  // your code goes here
  let getPeopleCount=getPeople.houses.reduce((acc,curr)=>{
 
        acc[curr.name]=curr.people.length
      
      return acc
  },{})
 
let order=Object.keys(getPeopleCount).sort().reduce((acc,curr)=>{
  acc[curr]=getPeopleCount[curr]
  return acc
},{})
return order
}

function everyone(data) {
  let arr=[]
  data.houses.map((names)=>{
    (names.people).map((ele)=>{
      arr.push(ele["name"])
    })
  })
  return arr
  
}

function nameWithS(data) {
  // your code goes here
  let arr=[]
  data.houses.map((objectName)=>{
    objectName.people.map((ele)=>{
      arr.push(ele["name"])
    })
  })
  
  let arr2=arr.filter((key)=>{
    if(key.includes("s")||key.includes("S")){
      return true
    }
  })
  return arr2
}

function nameWithA(data) {
  // your code goes here
   // your code goes here
   let arr=[]
   data.houses.map((objectName)=>{
     objectName.people.map((ele)=>{
       arr.push(ele["name"])
     })
   })
   
   let arr2=arr.filter((key)=>{
     if(key.includes("a")||key.includes("A")){
       return true
     }
   })
   return arr2
}

function surnameWithS(data) {
  let arr=[]
  data.houses.map((key)=>{
      key.people.map((key)=>{
        arr.push(key["name"])
      })
     })
    
    let finalArr=arr.filter((key)=>{
      // console.log(key.split(" "))
      return key.split(" ")[1][0]=="S"
    })
    return finalArr
}

function surnameWithA(data) {
  // your code goes here
  let arr=[];
  data.houses.map((key)=>{
    key.people.map((key)=>{
      arr.push(key["name"])
    })
   })
  
  let finalArr=arr.filter((key)=>{
    // console.log(key.split(" "))
    return key.split(" ")[1][0]=="A"
  })
  return finalArr
}

function peopleNameOfAllHouses(data) {
 finalData= data.houses.reduce((acc,key)=>{
  acc[key["name"]]=[]
  key["people"].map((ele)=>{
    acc[key["name"]].push(ele["name"])
    
  })
  return acc
  },{})
  return finalData
}

// Testing your result after writing your function
console.log(countAllPeople(got));
// // Output should be 33

console.log(peopleByHouses(got));
// // Output should be
// //{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone(got));
// // Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(got), 'with s');
// // Output should be
// // ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA(got));
// // Output should be
// // ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(got), 'surname with s');
// // Output should be
// // ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA(got));
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses(got));
// // Output should be
// // {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}
