// 1. Create a promise. Have it resolve with a value of `Promise Resolved!` in resolve after a delay of 1000ms, using `setTimeout`.
// Print the contents of the promise after it has been resolved by passing `console.log` to `.then`

let firstQustion = new Promise((resolve, reject) => {
  setTimeout(() => {
    let error = false;
    if (!error) {
      resolve("Promise Resolved");
    } else {
      reject("Error Found");
    }
  }, 1000);
});
firstQustion
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.log(err);
  });
