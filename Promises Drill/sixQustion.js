// Write a funtion named `wait` that accepts `time` in ms and executes the function after the given time.

let promise = new Promise((resolve, reject) => {
  function wait(time) {
    setTimeout(() => {
      console.log("Promis resovled after 1 second");
    }, time);
  }
  resolve(wait(1000));
});
promise.then((data) => {
  return data;
});
