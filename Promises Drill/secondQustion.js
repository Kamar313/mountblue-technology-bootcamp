// Create another promise. Now have it reject with a value of `Rejected Promise!` without using `setTimeout`.
// Print the contents of the promise after it has been rejected by passing console.log to `.catch`
let firstQustion = new Promise((resolve, reject) => {
  let error = true;
  if (!error) {
    resolve("Promise Resolved");
  } else {
    reject("Error Found");
  }
});
firstQustion
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.log(err);
  });
