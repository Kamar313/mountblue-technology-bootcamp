// This challenge we'll chain promises together using `.then` Create two variables: `firstPromise` and `secondPromise`.
// Set `secondPromise` to be a promise that resolves to "Second!". Set `firstPromise` to be a promise that resolves to `secondPromise`. Call the firstPromise with a `.then`, which will return the secondPromise promise. Then print the contents of the promise after it has been resolved by passing `console.log` to `.then
let secondPromis = new Promise((resolve, reject) => {
  resolve("second");
});
let firstPromis = new Promise((resolve, reject) => {
  resolve(secondPromis);
});
firstPromis
  .then((data) => {
    console.log(data);
  })
  .catch((err) => {
    console.error(err);
  });
