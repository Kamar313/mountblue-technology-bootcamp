// Write a basic implementation of `Promise.all` that accepts an array of promises
// and return another array with the data coming from all the promises.
// Make sure if any of the Promise gets rejected throw error.
// Only when all the promises are fulfilled resolve the promise.

let promise1 = new Promise((resolve, reject) => {
  resolve("1st Promiss resolve");
});
let promise2 = new Promise((resolve, reject) => {
  resolve("2nd Promiss resolve");
});
let promise3 = new Promise((resolve, reject) => {
  resolve("3rd Promiss resolve");
});

Promise.all([promise1, promise2, promise3]).then((data) => {
  console.log(data);
});
