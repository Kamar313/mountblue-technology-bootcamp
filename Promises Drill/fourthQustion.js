// What will be the output of the code below.

console.log("A"); //it will excute first in syncronise

// Asynchronous code finises in 0 seconds (Callback Queue)
setTimeout(() => console.log("B"), 0); //its call back it is less prority than promises.

// A promise that resolves right away (Microtask Queue)
Promise.resolve().then(() => console.log("C")); //it will excute third because promises will excute first because its more priroty then callback.

console.log("D"); // it will excute second in syncronise

//first Output is A
//second output is D
//third output is C
//fourth output is B
