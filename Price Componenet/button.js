function checkbox() {
  let checkBox = document.getElementById("input-button");
  let priceOne = document.querySelector(`.price1`);
  let priceTwo = document.querySelector(`.price2`);
  let priceThree = document.querySelector(`.price3`);
  if (checkBox.checked == true) {
    priceOne.innerHTML = "&dollar;19.99";
    priceTwo.innerHTML = "&dollar;24.99";
    priceThree.innerHTML = "&dollar;39.99";
  } else {
    priceOne.innerHTML = "&dollar;199.99";
    priceTwo.innerHTML = "&dollar;249.99";
    priceThree.innerHTML = "&dollar;399.99";
  }
}
