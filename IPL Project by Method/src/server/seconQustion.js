function getWinCountPerYear(matches){
    //let yearAndWinnerCount={};
   return matches.reduce((acc,curr)=>{
        if(!acc[curr["season"]]){
            acc[curr["season"]]={};
            acc[curr["season"]][curr["winner"]]=1
        }else if(!acc[curr["season"]][curr["winner"]]){   //if fisrt condition not satisfy.
            acc[curr["season"]][curr["winner"]]=1
        }
        else{
            acc[curr["season"]][curr["winner"]]+=1
        }
        return acc
    },{})
}
module.exports=getWinCountPerYear;