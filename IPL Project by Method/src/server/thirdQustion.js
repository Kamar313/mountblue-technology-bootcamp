function countExtraRun(matche,deliveri){
    let arr=[]
    matche.filter((ele)=>{
        if(ele.season==2016){
            arr.push(ele.id)
        }
    })
    
    let deliveyArr=[]
    deliveri.filter((ele)=>{
        if(arr.includes(ele.match_id)){
            deliveyArr.push(ele)
        }
    })

    let finalObject=deliveyArr.reduce((acc,curr)=>{
        let extrarun=parseInt(curr["extra_runs"])
        if(acc[curr["bowling_team"]]){
            acc[curr["bowling_team"]]+=extrarun
        }else{
            acc[curr["bowling_team"]]=extrarun
        }
        return acc
    },{})
    
    return finalObject
}

module.exports=countExtraRun;