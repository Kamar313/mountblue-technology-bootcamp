function maximumManOftheMatch(match){


    let countManoftheMatch=match.reduce((acc,curr)=>{
        let seasons=curr["season"];
        let manOfMatch=curr["player_of_match"]
        if(!acc[seasons]){
                acc[seasons]={}
                    acc[seasons][manOfMatch]=1
                }else if(!acc[seasons][manOfMatch]){
                    acc[seasons][manOfMatch]=1
                }else{
                    acc[seasons][manOfMatch]+=1
             }
             return acc
},{})

    let object={}
    
    for(let season in countManoftheMatch){
        let countarr=Object.entries(countManoftheMatch[season])
      //   console.log(countarr)
        let sortedArr=countarr.sort((a,b)=>b[1]-a[1])
        // console.log(sortedArr)
        object[season]=sortedArr[0][0]
    }
    return object
  
}
module.exports=maximumManOftheMatch