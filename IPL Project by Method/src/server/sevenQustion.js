function strikerateOfBatsman(player,matches,deliveries){
    const matchId = matches.reduce((acc, curr) => {
        acc[curr.id] = curr.season;
        return acc;
    }, {});

    const totalRun = deliveries.reduce((acc, delivery) => {
        const year = matchId[delivery.match_id];
        acc[delivery.batsman] ?? (acc[delivery.batsman] = {});
        acc[delivery.batsman][year] ?? (acc[delivery.batsman][year] = { total_runs: 0, total_balls: 0 });
        acc[delivery.batsman][year]['total_runs'] += isNaN(delivery.total_runs) ? 0 : Number(delivery.total_runs);
        acc[delivery.batsman][year]['total_balls']++;

        return acc;
    }, {});

    const getData = Object.keys(totalRun).reduce((avragePerYear, striker) => {
        Object.keys(avragePerYear[striker]).reduce((avragePerYar, year) => {
            const totalRuns = avragePerYar[striker][year]['total_runs'];
            const totalBalls = avragePerYar[striker][year]['total_balls'];

            avragePerYar[striker][year] = Number(((totalRuns / totalBalls) * 100).toFixed(2));

            return avragePerYar;
        }, avragePerYear);

        return avragePerYear;
    }, totalRun); //

    for(let key in getData){
        if(key==player){
            return key,getData[key]
        }
    }

}
module.exports=strikerateOfBatsman