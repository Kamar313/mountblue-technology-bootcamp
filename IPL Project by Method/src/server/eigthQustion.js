function getdismissed(seres){
    let obj=seres.reduce((acc,curr)=>{
        let boler=curr["bowler"]
        let out=curr["player_dismissed"]
        if(acc[out]!=='')
            if(!acc[boler]){
                acc[boler]={}
                acc[boler][out]=1
        }else if(!acc[boler][out]){
            acc[boler][out]=1
        }else{
            acc[boler][out]+=1
        }
        delete acc[boler]['']
        return acc
    },{})
    
    let arr=[]
    for(let key in obj){
        arr.push(Object.values(obj[key]))
    }
    let maxwiket=Math.max(...arr.flat())
    // console.log(...arr)
    let arr2=[]
    Object.keys(obj).map((batsman) => {
        const bowlersWhoDismissed = Object.keys(obj[batsman]).filter((bowler) => {
            return obj[batsman][bowler] === maxwiket;
        })

        if(bowlersWhoDismissed.length>0){
        arr2.push(batsman,bowlersWhoDismissed,maxwiket)
        }
    });
    let flatArr=arr2.flat()
    return flatArr
}
module.exports=getdismissed