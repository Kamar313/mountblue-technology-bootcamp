const nestedArray = [1, [2], [[3]], [[[4]]]];
function flatten(ele){
   let output=[];
   recurcive(0,ele,output) //That is recurson function.
   return output;
}
function recurcive(index,ele,output){
    if(index>=ele.length)return;
    
    if(Array.isArray(ele[index])){
        recurcive(0,ele[index],output)
    }else{
        output.push(ele[index])
    }
    recurcive(index+1,ele,output)
}
console.log(flatten(nestedArray));