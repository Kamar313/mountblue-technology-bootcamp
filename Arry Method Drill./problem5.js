function filter(elements,cb){
    let arry=[]
    for(let index=0;index<elements.length;index++){
        if(cb(elements[index])){
            arry.push(elements[index])
        }
    }
    return arry
}
module.exports=filter;