function counterFactory(count){
    return {
        incriment: function(){
            return count+1
        },
        decriment: function(){
           return count-1
        }
    }
}
module.exports=counterFactory;