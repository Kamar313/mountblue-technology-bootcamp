# 1) Flat.

## What Paramiter its Accept?
⮕ Its using one Paramiter for depth of arr and its accept number argument.

## What it Will return?
⮕ A new array with the sub-array elements concatenated into it.

## Example
```
let arr=[1,2,3,4,[5,6]];
let newArr=arr.flat(1);
console.log(newArr);
=>[1,2,3,4,5,6]
```

## Write in your own words what flat means?
⮕ Its using one Paramiter for depth of arr and its accept 
number argument and return new arry and not change orignal array.

## Does it mutate the original array?
⮕ Its not Muteting orignal array.

# 2) Concat

## What Paramiter its Accept?
⮕ (any) number of values (number, string, boolean, array, null, undefined, object and function etc).

## What it Will return?
⮕ a single Array consisting of by all the values passed as parameters in the same order.

## Example
```
let numbers = [1, 2, 3];
let connect=numbers.concat(4);
console.log(connect)=>[1,2,3,4]
```
## Write in your own words what contact means?
⮕ concat accepts n number of values and returns one array with all the values in same order. It does not change the original array.

## Does it mutate the original array?
⮕ Its not Muteting orignal array.

# 3) Push.

## What Paramiter its Accept?
⮕ Its Accept N argument and it accept number, string, array.

## What it Will return?
⮕ It return that array with updated.

## Example
```
let array=[1,2,3,5]
array.push(6)
console.log(array)=>[1,2,3,4,5,6]
```

## Write in your own words what push means?
⮕ It accept n argument and modify orignal array.

## Does it mutate the original array?
⮕ Yes

# 4)Pop

## What Paramiter its Accept?
⮕ No its not taking paramiter.

## What it Will return?
⮕ It return that array with updated.

## Example
```
let array=[1,2,3,5]
array.pop()
console.log(array)=>[1,2,3,4]
```

##  Write in your own words what pop means?
⮕ Its Not accepting argument and its remove last element of array.

## Does it mutate the original array?
⮕ Yes

# 5) Shift

## What Paramiter its Accept?
⮕ No its not taking paramiter.

## What it Will return?
⮕ It return that array with updated.

## Example
```
let array=[1,2,3,5]
array.shift()
console.log(array)=>[2,3,4,5]
```

##  Write in your own words what shift means?
⮕ Its Not accepting argument and its remove first element of array.

## Does it mutate the original array?
⮕ Yes

# 6) Unshift

## What Paramiter its Accept?
⮕ Yes its taking paramiter any and n.

## What it Will return?
⮕ It return that array with updated.

## Example
```
let array=[1,2,3,5]
array.unshift(6,7,8,9)
console.log(array)=>[6,7,8,9,1,2,3,4,5]
```

##  Write in your own words what unshift means?
⮕ Its accepting argument and its add  element in start of array.

## Does it mutate the original array?
⮕ Yes.

# 7) IndexOf

## What Paramiter its Accept?
⮕ Yes its taking paramiter string and starting value.

## What it Will return?
⮕ it will return where that occurs.

## Example
```
let array=[1,2,3,5]
let text=array.indexof(3)
console.log(text)=>2
```

##  Write in your own words what indexof means?
⮕ Its Finding the value and return that index if not found return -1.

## Does it mutate the original array?
⮕ Its not Muteting orignal array.

# 8) lastIndexOf

## What Paramiter its Accept?
⮕ Yes its taking paramiter string and starting value.

## What it Will return?
⮕ it will return where that occurs.

## Example
```
let array=[1,2,3,5]
let text=array.lastIndexOf(3)
console.log(text)=>5
```

##  Write in your own words what lastIndexOf means?
⮕ Its Finding the value and return that index if not found return -1.

## Does it mutate the original array?
⮕ Its not Muteting orignal array.

# 9) Includes

## What Paramiter its Accept?
⮕ Yes its taking Value for search.

## What it Will return?
⮕ it will reaturn boolean value if value is persent

## Example
```
let array=[1,2,3,5]
let text=array.includes(3)
console.log(text)=>true
```

##  Write in your own words what includes means?
⮕ It is finding that value and if it is persent return true if not persent return false.

## Does it mutate the original array?
⮕ Its not Muteting orignal array.

# 10) Reverse

## What Paramiter its Accept?
⮕ It will take array as paramiter.

## What it Will return?
⮕ It Will Reverse Arry.

## Example
```
let array=[1,2,3,5]
array.reverse(array)
console.log(array)=>[5,3,2,1]
```

##  Write in your own words what reverse means?
⮕ It will reverse orignal array and return it.

## Does it mutate the original array?
⮕ Yes.

# 11) Splice

## What Paramiter its Accept?
⮕ Its taking starting index and deleting index.

## What it Will return?
⮕ It will return deleted array.

## Example
```
let array=[1,2,3,5]
let splice=array.splice(2,0,3)
console.log(splice)=>[] Not deleted
console.log(array)=>[1,2,3,3,5]
```

##  Write in your own words what splice means?
⮕ Its return deleted arry.

## Does it mutate the original array?
⮕ Yes.

# 12) Slice

## What Paramiter its Accept?
⮕ Its taking starting index and ending index.

## What it Will return?
⮕ A new array containing the extracted elements.

## Example
```
let array=[1,2,3,5]
let slice=array.slice(2,3)
console.log(array)=>[3,5]
```

##  Write in your own words what slice means?
⮕ Its return new array with remaining element.

## Does it mutate the original array?
⮕ No

# 13) ForEach

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕ It Will return Undifine.

## Example
```
const arraySparse = [1, 3, /* empty */, 7];
let numCallbackRuns = 0;
arraySparse.forEach((element) => {
console.log({ element });
numCallbackRuns++;
});

console.log({ numCallbackRuns });
```

##  Write in your own words what ForEach means?
⮕ The forEach() method is an iterative method. It calls a provided callbackFn function once for each element in an array in ascending-index order.

## Does it mutate the original array?
⮕ No

# 14) Map

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕  A new array with each element being the result of the callback function.

## Example
```
const numbers = [1, 4, 9];
const roots = numbers.map((num)
=> Math.sqrt(num));

// roots is now     [1, 2, 3]
// numbers is still [1, 4, 9]
```

##  Write in your own words what Map means?
⮕   The map() method is an iterative method. It calls a provided callbackFn function once for each element in an array and constructs a new array from the results.

## Does it mutate the original array?
⮕ No

# 15) Filter

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕  A shallow copy of a portion of the given array, filtered down to just the elements from the given array that pass the test implemented by the provided function.

## Example
```
function isBigEnough(value) {
return value >= 10;
}
const filtered = [12, 5, 8, 130, 44].filter(isBigEnough);
// filtered is [12, 130, 44]
```

##  Write in your own words what Filter means?
⮕   The filter() method is an iterative method. It calls a provided callbackFn function once for each element in an array, and constructs a new array of all the values for which callbackFn returns a truthy value.

## Does it mutate the original array?
⮕ No

# 16) find

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕  The first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.

## Example
```
const inventory = [
{ name: "apples", quantity: 2 },
{ name: "bananas", quantity: 0 },
{ name: "cherries", quantity: 5 },
];

function isCherries(fruit) {
return fruit.name === "cherries";
}

console.log(inventory.find(isCherries));
=> { name: 'cherries', quantity: 5 }

```

##  Write in your own words what Find means?
⮕   The find() method is an iterative method. It calls a provided callbackFn function once for each element in an array in ascending-index order, until callbackFn returns a truthy value. 

## Does it mutate the original array?
⮕ No

# 17) FindIndex
## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕  The first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.

## Example
```
function isPrime(element) {
if (element % 2 === 0 || element < 2) {
            return false;
}
for (let factor = 3; factor <= Math.sqrt(element); factor += 2) {
    if (element % factor === 0) {
            return false;
         }
    }
        return true;
}

console.log([4, 6, 8, 9, 12].findIndex(isPrime)); // -1, not found
console.log([4, 6, 7, 9, 12].findIndex(isPrime)); // 2 (array[2] is 7)

```

##  Write in your own words what FindIndex means?
⮕   The findIndex() is an iterative method. It calls a provided callbackFn function once for each element in an array in ascending-index order, until callbackFn returns a truthy value. 


## Does it mutate the original array?
⮕ No


# 18) Some

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕ True if the callback function returns a truthy value for at least one element in the array. Otherwise, false.

## Example
```
function isBiggerThan10(element, index, array) {
    return element > 10;
}

[2, 5, 8, 1, 4].some(isBiggerThan10);  // false
[12, 5, 8, 1, 4].some(isBiggerThan10); // true

```

##  Write in your own words what Some means?
⮕ The some() method is an iterative method. It calls a provided callbackFn function once for each element in an array, until the callbackFn returns a truthy value.


## Does it mutate the original array?
⮕ No

# 19) Every

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and index and Arr.

## What it Will return?
⮕ True if callbackFn returns a truthy value for every array element. Otherwise, false.

## Example
```
function isBigEnough(element, index, array) {
    return element >= 10;
}
[12, 5, 8, 130, 44].every(isBigEnough);   // false
[12, 54, 18, 130, 44].every(isBigEnough); // true

```

##  Write in your own words what every means?
⮕ The every() method is an iterative method. It calls a provided callbackFn function once for each element in an array, until the callbackFn returns a falsy value.

## Does it mutate the original array?
⮕ No

# 20) Sort
## What Paramiter its Accept?
⮕ Its take 1st argument and 2nd argument.

## What it Will return?
⮕ It will return orignal array in sorted array.

## Example
```
const stringArray = ['Blue', 'Humpback', 'Beluga'];
stringArray.sort(); // ['Beluga', 'Blue', 'Humpback']

```

##  Write in your own words what Sort means?
⮕ Its Compare 1st value with second value and sort it

## Does it mutate the original array?
⮕ Yes

# 21) Reduce

## What Paramiter its Accept?
⮕ Its take callBack function and currentValue and CurrentValue

## What it Will return?
⮕ The value that results from running the "reducer" callback function to completion over the entire array.

## Example
```
const array = [15, 16, 17, 18, 19];

function reducer(accumulator, currentValue, index) {
const returns = accumulator + currentValue;
console.log(
`accumulator: ${accumulator}, currentValue: ${currentValue}, index: ${index}, returns: ${returns}`,
    );
    return returns;
}
array.reduce(reducer);

```

##  Write in your own words what reduce means?
⮕ he reduce() method is an iterative method. It runs a "reducer" callback function over all elements in the array, in ascending-index order, and accumulates them into a single value.

## Does it mutate the original array?
⮕ No