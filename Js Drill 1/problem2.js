// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
//Execute a function to find what the make and model of the last car in the inventory is?
//Log the make and model into the console in the format of:

function seconQustion(carsArry){
    if(carsArry.length>0){
    let lastObject=carsArry[carsArry.length-1]
    return `Last car is a ${lastObject.id+" "+lastObject["car_make"]+" "+lastObject["car_model"]} `
}
return "No Arry"
}

module.exports=seconQustion;
