// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. 
//Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
//Then log the car's year, make, and model in the console log in the format of:

function firstQustion(carsArry){
    if(carsArry.length>0){
    for(let i=0;i<carsArry.length;i++){
        if(carsArry[i].id==33){
            let cars=carsArry[i]
            return `Car 33 is a ${cars["car_year"]} ${cars["car_make"]} ${cars["car_model"]}`
        }
    }
}
return "No Arry"
}
module.exports=firstQustion;

