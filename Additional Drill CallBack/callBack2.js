// const boardsData = require("./Json Data/boards.json");
const list = require("./Json Data/lists.json");
const data = require("./Json Data/boards.json");

function findBoardIdFromList(id, callback) {
  setTimeout(() => {
    let finddata = data.filter((getboardId) => getboardId.id == id);
    let matchKey = "";
    for (let key in list) {
      if (key == finddata[0].id) {
        //If key match than it will return that key.
        matchKey = key; //Gatting Key from list.
      }
    }
    let result = list[matchKey];
    callback(result);
  }, 2000);
}
module.exports = findBoardIdFromList;
