function maximumManOftheMatch(match){
    let countManoftheMatch={}
    for(let i=0;i<match.length;i++){
        let seasons=match[i]["season"];
        let manOfMatch=match[i]["player_of_match"]
        if(!countManoftheMatch[seasons]){
            countManoftheMatch[seasons]={}
            countManoftheMatch[seasons][manOfMatch]=1
        }else if(!countManoftheMatch[seasons][manOfMatch]){
            countManoftheMatch[seasons][manOfMatch]=1
        }else{
            countManoftheMatch[seasons][manOfMatch]+=1
        }
    }
    let object={}
    for(let season in countManoftheMatch){
        let countarr=Object.entries(countManoftheMatch[season])
      //   console.log(countarr)
        let sortedArr=countarr.sort((a,b)=>b[1]-a[1])
        // console.log(sortedArr)
        object[season]=sortedArr[0][0]
    }
    return object
  
}
module.exports=maximumManOftheMatch