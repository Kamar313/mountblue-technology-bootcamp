function getdismissed(seres){
    let obj={}
    for(let i=0;i<seres.length;i++){
        let boler=seres[i]["bowler"]
        let out=seres[i]["player_dismissed"]
        if(obj[out]!=='')
        if(!obj[boler]){
            obj[boler]={}
            obj[boler][out]=1
        }else if(!obj[boler][out]){
            obj[boler][out]=1
        }else{
            obj[boler][out]+=1
        }
        delete obj[boler]['']
       
    }
    let arr=[]
    for(let key in obj){
        arr.push(Object.values(obj[key]))
    }
    let maxwiket=Math.max(...arr.flat())
    // console.log(...arr)
    let arr2=[]
    Object.keys(obj).map((batsman) => {
        const bowlersWhoDismissed = Object.keys(obj[batsman]).filter((bowler) => {
            return obj[batsman][bowler] === maxwiket;
        })

        if(bowlersWhoDismissed.length>0){
        arr2.push(batsman,bowlersWhoDismissed,maxwiket)
        }
    });
    let flatArr=arr2.flat()
    return flatArr
}
module.exports=getdismissed