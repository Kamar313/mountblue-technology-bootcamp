function getWinCountPerYear(matches){
    let yearAndWinnerCount={};
   for(let i=0;i<matches.length;i++){
       let year=matches[i].season
       let team=matches[i].winner
       if(!yearAndWinnerCount[year]){
           yearAndWinnerCount[year]={};
           yearAndWinnerCount[year][team]=1
       }else if(!yearAndWinnerCount[year][team]){   //if fisrt condition not satisfy.
           yearAndWinnerCount[year][team]=1
       }
       else{
           yearAndWinnerCount[year][team]+=1
       }
   }
   return yearAndWinnerCount
}
module.exports=getWinCountPerYear;