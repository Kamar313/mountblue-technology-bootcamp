// 3rd Party packge.
const csvtojson=require('csvtojson')    //Node packge to convert csv File.

// Local packge
const fs = require("fs") // FS means File System

//Csv file path
const deliveriesPath= "IPL Project/src/data/deliveries.csv"; //Reletive path
const matchesPath= "IPL Project/src/data/matches.csv";


csvtojson()
.fromFile(deliveriesPath)       //After providing path it return promise
.then((jsonObj)=>{              //That promise will return Array object.
    fs.writeFile("IPL Project/src/public/output/deliverie.json", //it will create json file with array object.
    JSON.stringify(jsonObj,null,2),
    (err)=>{if(err){
            throw err              //If error is found it will throw error
            }
        }          
    )
  }
)

//Convert into Json file 
csvtojson()
.fromFile(matchesPath)
.then((jsonObj)=>{
    fs.writeFile("IPL Project/src/public/output/matches.json",
    JSON.stringify(jsonObj,null,2),
    (err)=> {if(err){
        throw err
            }
        }
    )
  }
)

//File path for json File
const matches=require("../public/output/matches.json");
const deliverie=require("../public/output/deliverie.json");


//First Qustion Output
let getMatchesPerYear=require('./firstQustion')
let finalCount=getMatchesPerYear(matches)
fs.writeFileSync("IPL Project/src/public/output/finalCount.json",JSON.stringify(finalCount,null,2))

//Second Qustion Output
let getMatches=require('./seconQustion')
let getCountOfEveryYear=getMatches(matches)
fs.writeFileSync("IPL Project/src/public/output/getwinnerCount.json",JSON.stringify(getCountOfEveryYear,null,2))

//Third Qustion Output
let countExtraRun=require('./thirdQustion')
let countExtraRunIn2016=countExtraRun(matches,deliverie)
fs.writeFileSync("IPL Project/src/public/output/countExtraRunIn2016.json",JSON.stringify(countExtraRunIn2016,null,2))

//Fourth Qustion.
let findbowlerEconmy=require('./forthQustion')
let topBoalerEconmy=findbowlerEconmy(matches,deliverie)
fs.writeFileSync("IPL Project/src/public/output/topBoalerEconmy.json",JSON.stringify(topBoalerEconmy,null,2))

//Fifth Qustion.
let findWintoss=require('./fifthQustion')
let findTossAndWin=findWintoss(matches,deliverie)
fs.writeFileSync("IPL Project/src/public/output/findTossAndWin.json",JSON.stringify(findTossAndWin,null,2))

//Six Qustion
let maximumManOftheMatch=require('./sixQustion')
let findMaxManOfMatch=maximumManOftheMatch(matches)
fs.writeFileSync("IPL Project/src/public/output/findMaxManOfMatch.json",JSON.stringify(findMaxManOfMatch,null,2))


//Seven Qustion
let stikeRate=require('./sevenQustion')
let StikeaRateOfBatsman=stikeRate("S Dhawan",matches,deliverie)
fs.writeFileSync("IPL Project/src/public/output/StikeaRateOfBatsman.json",JSON.stringify(StikeaRateOfBatsman,null,2))

//Eight Qustion
let dismissCount=require('./eigthQustion')
let getDismissedData=dismissCount(deliverie)
fs.writeFileSync("IPL Project/src/public/output/getDismissedData.json",JSON.stringify(getDismissedData,null,2))

//Nine Qustion
let econmy=require('./nineQustion')
let bestEconmy=econmy(deliverie)
fs.writeFileSync("IPL Project/src/public/output/bestEconmy.json",JSON.stringify(bestEconmy,null,2))